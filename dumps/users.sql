BEGIN;

-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE "public"."users" ( 
	"id" Integer NOT NULL,
	"email" Character Varying( 150 ),
	"first_name" Character Varying( 150 ),
	"last_name" Character Varying( 150 ),
	"auth_key" Character Varying( 255 ),
	"access_token" Character Varying( 255 ),
	"password" Character Varying( 255 ),
	PRIMARY KEY ( "id" ),
	CONSTRAINT "unique_id" UNIQUE( "id" ) );
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE FIELD "role" -----------------------------------------
ALTER TABLE "public"."users" ADD COLUMN "role" Integer[][] DEFAULT 0 NOT NULL;
-- -------------------------------------------------------------

COMMIT;
